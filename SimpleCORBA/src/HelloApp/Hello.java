package HelloApp;


/**
* HelloApp/Hello.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从./hello
* 2018年5月1日 星期二 下午09时34分52秒 CST
*/

public interface Hello extends HelloOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity 
{
} // interface Hello
