# Java/CORBA

---

本文HelloCorba参考 [Getting Started with JavaTM IDL][1]

## 说在前面

> Java TM IDL is a technology for distributed objects--that is, objects interacting on different platforms across a network. Java IDL enables objects to interact regardless of whether they're written in the Java programming language or another language such as C, C++, COBOL, or others. 

 Common Object Request Brokerage Architecture (CORBA)：公共对象请求代理体系结构
 Object Request Broker：对象请求代理

 This figure shows how a one-method distributed object is shared between a CORBA client and server to implement the classic "Hello World" application. 

 ![image_1ccdtjikt14od1lvr1nlrm1j59h9.png-3.8kB][2]


# HelloCorba说明

### 1. 新建文件Hello.idl
内容为：
    
    module HelloApp
    {
      interface Hello
      {
        string sayHello();
        oneway void shutdown();
      };
    };

### 2. 输入命令

    idlj -fall Hello.idl

安装了jdk就会有idlj，idlj跟javac是在同一个目录的；
如果出现异常`java.io.FileNotFoundException: hello.idl (No such file or directory)`，可尝试命令` idlj -fall ./Hello`
这样我们得到一个文件夹，里面有6个文件，这6个文件是什么来的呢？我们先接着往下看

    _HelloStub.java
    Hello.java
    HelloHelper.java
    HelloHolder.java
    HelloOperations.java
    HelloPOA.java

`HelloOperations.java`里面才上我们真正需要的操作，我们需要实现的是抽象类`HelloPOA`，其实就是实现接口`HelloOperations`

### 3. HelloServer
代码在这：
https://docs.oracle.com/javase/1.5.0/docs/guide/idl/tutorial/GSserver.html

### 4. HelloClient
代码在这：
https://docs.oracle.com/javase/1.5.0/docs/guide/idl/tutorial/GSapp.html

### 5. 运行
参照：https://docs.oracle.com/javase/1.5.0/docs/guide/idl/tutorial/GScompile.html

作者运行环境为：macos:10.13.4 (17E199)  JDK1.8  iterm2

1.启动orbd

    orbd -ORBInitialPort 1050 -ORBInitialHost localhost&

（末尾的&是指以守护线程的方式启动）

2.先开始server

    java HelloServer -ORBInitialPort 1050 -ORBInitialHost localhost&

3.最后开启client

    java HelloClient -ORBInitialPort 1050 -ORBInitialHost localhost

4.结果

Hello world !!


# ToDoListCorba 设计思路

## 写两个module

定义操作

    module centerModule{
      interface CenterService{
          boolean login( in string name, in string password);
          boolean register(in string name,in  string password);
          boolean save();
      };
  };

定义用户操作

       module userModule{   
            interface UserService{   
                boolean add(in string startTime,in string endTime,in string item);
    	        string query(in string startTime,in string endTime);
    	        boolean delete(in string item);
    	        boolean clear();
    	        string show();
    	        boolean save(in string name);
    	        void init(in string name);
            };   
    };  


在上面两个文件所在的目录使用`idlj`命令编译，输入命令`idlj -fall yourname.idl`
ok，成功得到下面文件

    _OperationServiceStub.java
    OperationService.java
    OperationServiceHelper.java
    OperationServiceHolder.java
    OperationServiceOperations.java
    OperationServicePOA.java

同理，我们可以得到

    _UserServiceStub.java
    UserService.java
    UserServiceHelper.java
    UserServiceHolder.java
    UserServiceOperations.java
    UserServicePOA.java


## 实现2个POA

所有的操作都源于service，server端暴露的也只有service的接口，其余的实现都是隐蔽的，所以，关键是实现好service。

`CenterService`实现的功能有：登录、注册、保存
有一个成员变量`Map<String,User>`

 1. 所有的注册用户使用一个`Map<String,User>`来管理，登录注册都是对`Map<String,User>`的操作
 2. 保存是将`Map<String,User>`序列化到本地的data/centerService文件，通过读取data/centerService文件来反序列化`Map<String,User>`，实现数据的持久化

`UserService`实现的功能有：添加、查询、删除、清除、保存。有一个成员变量`List<Item>`

 1. CURD操作是针对`List<Item>`来展开的
 2. 保存是将`List<Item>`序列化到本地的data/userName文件，为每一个注册的用户都独立保存`List<Item>`，通过读取data/userName文件来反序列化`List<Item>`

## server

`ToDoListServer`是本程序的`server`，作用有2个：

 1. 第一个作用是提供`CenterService`，用于登录注册
 2. 第二个作用是为进行登录、注册操作的用户提供`userService`。

## client
`ToDoListClient是`是本程序的`client`，作用是获取服务端的`CenterService`为用户提供登录、注册功能，然后为注册、登录成功的用户提供`UserService`功能

## 基本原理分析

基本思路跟RPC是一样的，首先在服务端新建一个对象，注册好了，然后再客户端获得这个对象的引用，当调用这个引用的方法的时候，实际上会调用到客户端的对象，然后再把结果返回给客户端的对象引用。

在`ToDoListServer`里面，通过`COBRA`将`centerServiceImpl`对象绑定到`COBRA`的根命名上下文（root naming context），它有个名字叫做`NameService`，然后我们给`centerServiceImpl`对象取个名字，就叫`centerService`，然后把`CenterServiceImpl`绑定到`NameService`下面。后面我们就可以通过`centerService`获取到我们预先绑定的`centerServiceImpl`对象了。

具体实现方案看代码`ToDoListServer.init()`和`ToDoListClient.init`


## 演示流程

 1. 首先必须启动orbd，命令：`orbd -ORBInitialPort 1050 -ORBInitialHost localhost&`
 2. 启动`ToDoListServer`
![image_1cd01p18f35fo4r1q0m7jkl0v19.png-13.5kB][3]
 3. 启动`ToDoListClient`
 ![image_1ccsbmlqh115p1o0715vi1ugb1onk9.png-35.9kB][4]
 4. 注册账号hello3 ，密码hello3
![image_1cd020jip1q9d1nesoseoijkn26.png-12.4kB][5]
 5. 添加一些数据，时间的输入要求不含有中文
    ![image_1cd026nqjlpipkcs15qgsnn216.png-44.6kB][6]
 6. 开始查询，查询根据开始时间和结束时间的范围查询
 ![image_1cd02685ok35ho611901qa10jbp.png-27.5kB][7]
 7. 删除看看
 ![image_1cd0298gb1gc0nn91cqp1hm7iq61j.png-19.5kB][8]
 8. 重启client，server，再次登录hello3账号
 ![image_1cd02br4891edcf9r61o4f1q5l20.png-35.5kB][9]

## 源码链接

[java-corba][10]

## 最后
感谢看到最后的你，喜欢文章就点个👍再走啦 ^-^


[1]: https://docs.oracle.com/javase/1.5.0/docs/guide/idl/GShome.html
[2]: http://static.zybuluo.com/Langdon/mph32wq36z2cd2qvatkmr98v/image_1ccdtjikt14od1lvr1nlrm1j59h9.png
[3]: http://static.zybuluo.com/Langdon/o8ozyjueruzc3x27l6fny22d/image_1cd01p18f35fo4r1q0m7jkl0v19.png
[4]: http://static.zybuluo.com/Langdon/viilnjfqr4kzo2913xwj5u34/image_1ccsbmlqh115p1o0715vi1ugb1onk9.png
[5]: http://static.zybuluo.com/Langdon/iaiwh706tjdp0p1i6tudgqob/image_1cd020jip1q9d1nesoseoijkn26.png
[6]: http://static.zybuluo.com/Langdon/qhk6kkc903bf4ln79ex8p92h/image_1cd026nqjlpipkcs15qgsnn216.png
[7]: http://static.zybuluo.com/Langdon/5m2g66hu4ng0w1fmgjs5z6rt/image_1cd02685ok35ho611901qa10jbp.png
[8]: http://static.zybuluo.com/Langdon/3zkltbksmee71kw0gqhwg0it/image_1cd0298gb1gc0nn91cqp1hm7iq61j.png
[9]: http://static.zybuluo.com/Langdon/wfet2gp9huxew6uumhecmiun/image_1cd02br4891edcf9r61o4f1q5l20.png
[10]: https://gitee.com/iss2015302580320/Java-CORBA

