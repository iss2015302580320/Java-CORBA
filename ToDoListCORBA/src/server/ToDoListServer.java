package server;

import java.util.Properties;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import centerModule.CenterService;
import centerModule.CenterServiceHelper;
import conf.Configuration;
import impl.CenterServiceImpl;
import impl.UserServiceImpl;
import userModule.UserService;
import userModule.UserServiceHelper;


public class ToDoListServer {
	
	private ORB orb;
	private POA rootpoa;
	private org.omg.CORBA.Object ref;
	private org.omg.CORBA.Object objRef;
	private NamingContextExt ncRef;
	public ToDoListServer() {
		
	}
	
	/**
	 * 运行之前请先在命令行以守护线程形式启动orbd
	 * orbd -ORBInitialPort 1050 -ORBInitialHost localhost&
	 * @param args
	 */
	
	public void init(String [] args) {
		try{
			 if (args==null || args.length == 0) {
				 System.out.println("args is null ~~~");
				 Properties props = new Properties();
				// 生成一个ORB，并初始化，这个和Server端一样  
			     props.put("org.omg.CORBA.ORBInitialPort", Configuration.getConfig("org.omg.CORBA.ORBInitialPort"));  
			     props.put("org.omg.CORBA.ORBInitialHost", Configuration.getConfig("org.omg.CORBA.ORBInitialHost"));
			  // create and initialize the ORB
				   this.orb = ORB.init(args, props);
			 }else {
				 System.out.println("args is not null ~~~");
				  this.orb = ORB.init(args, null);
			 }
		
		   // get reference to rootpoa & activate the POAManager
		   rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
		   rootpoa.the_POAManager().activate();

		   // create servant and register it with the ORB
		   CenterServiceImpl centerServiceImpl = new CenterServiceImpl();
		   centerServiceImpl.setORB(orb);
		   centerServiceImpl.setServer(this);//方便用户register

		   // get object reference from the servant
		   ref = rootpoa.servant_to_reference(centerServiceImpl);
		   CenterService href = CenterServiceHelper.narrow(ref);

		   // get the root naming context
		   objRef =
		       orb.resolve_initial_references("NameService");
		   // Use NamingContextExt which is part of the Interoperable
		   // Naming Service (INS) specification.
		   ncRef = NamingContextExtHelper.narrow(objRef);

		   // bind the Object Reference in Naming
		   NameComponent path[] = ncRef.to_name(Configuration.getConfig("service.center.name"));
		   ncRef.rebind(path, href);
		   System.out.println("centerService ready and waiting ...");
		   // wait for invocations from clients
		   orb.run();
		 }
		   catch (Exception e) {
		     System.err.println("ERROR: " + e);
		     e.printStackTrace(System.out);
		   }
			  
		   System.out.println("CenterService Exiting ...");
	}
	
	/**
	 * 添加以name命名的UserServiceImpl
	 * @param name
	 * @return
	 */
	public boolean registerService(String name) {
		// create servant and register it with the ORB
		   UserServiceImpl userServiceImpl = new UserServiceImpl(name);
		   userServiceImpl.setORB(orb); 
		  try {
			  // get object reference from the servant
			   ref = rootpoa.servant_to_reference(userServiceImpl);
			   UserService href = UserServiceHelper.narrow(ref);
			   // bind the Object Reference in Naming
			   NameComponent path[] = ncRef.to_name(name);
			   ncRef.rebind(path, href);
			   System.out.println("centerService ready and waiting ...");
			   return true;
		  }catch(Exception e) {
			  e.printStackTrace();
		  }
		return false;
	}
	
	public static void main(String args[]) {
		 ToDoListServer server = new ToDoListServer();
		 server.init(args);
	}
}
