package po;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = -2998707288951184222L;
	
	private String name;
	
	private String password;

	public User() {
		
	}
	
	public User(String name, String password) {
		super();
		this.name = name;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
