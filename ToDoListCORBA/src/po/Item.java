package po;

import java.io.Serializable;

public class Item implements Serializable{

	private static final long serialVersionUID = -9036114710187313266L;

	public Item(String startTime, String endTime, String value) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.value = value;
	}
			
	public Item() {
			
	}
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private String startTime;
	
	private String endTime;
	
	private String value;
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(startTime)
		.append("\t")
		.append(endTime)
		.append("\t")
		.append(value);
		return sb.toString();
	}
	
	
}
