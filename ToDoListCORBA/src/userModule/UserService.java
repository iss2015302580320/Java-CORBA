package userModule;


/**
* userModule/UserService.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从user.idl
* 2018年5月8日 星期二 下午01时27分38秒 CST
*/

public interface UserService extends UserServiceOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity 
{
} // interface UserService
