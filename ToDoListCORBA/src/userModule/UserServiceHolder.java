package userModule;

/**
* userModule/UserServiceHolder.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从user.idl
* 2018年5月8日 星期二 下午01时27分38秒 CST
*/

public final class UserServiceHolder implements org.omg.CORBA.portable.Streamable
{
  public userModule.UserService value = null;

  public UserServiceHolder ()
  {
  }

  public UserServiceHolder (userModule.UserService initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = userModule.UserServiceHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    userModule.UserServiceHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return userModule.UserServiceHelper.type ();
  }

}
