package userModule;


/**
* userModule/UserServiceOperations.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从user.idl
* 2018年5月8日 星期二 下午01时27分38秒 CST
*/

public interface UserServiceOperations 
{
  boolean add (String startTime, String endTime, String item);
  String query (String startTime, String endTime);
  boolean delete (String item);
  boolean clear ();
  String show ();
  boolean save (String name);
} // interface UserServiceOperations
