package userModule;


/**
* userModule/UserServiceHelper.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从user.idl
* 2018年5月8日 星期二 下午01时27分38秒 CST
*/

abstract public class UserServiceHelper
{
  private static String  _id = "IDL:userModule/UserService:1.0";

  public static void insert (org.omg.CORBA.Any a, userModule.UserService that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static userModule.UserService extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      __typeCode = org.omg.CORBA.ORB.init ().create_interface_tc (userModule.UserServiceHelper.id (), "UserService");
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static userModule.UserService read (org.omg.CORBA.portable.InputStream istream)
  {
    return narrow (istream.read_Object (_UserServiceStub.class));
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, userModule.UserService value)
  {
    ostream.write_Object ((org.omg.CORBA.Object) value);
  }

  public static userModule.UserService narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof userModule.UserService)
      return (userModule.UserService)obj;
    else if (!obj._is_a (id ()))
      throw new org.omg.CORBA.BAD_PARAM ();
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      userModule._UserServiceStub stub = new userModule._UserServiceStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

  public static userModule.UserService unchecked_narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof userModule.UserService)
      return (userModule.UserService)obj;
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      userModule._UserServiceStub stub = new userModule._UserServiceStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

}
