package userModule;


/**
* userModule/UserServicePOA.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从user.idl
* 2018年5月8日 星期二 下午01时27分38秒 CST
*/

public abstract class UserServicePOA extends org.omg.PortableServer.Servant
 implements userModule.UserServiceOperations, org.omg.CORBA.portable.InvokeHandler
{

  // Constructors

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static
  {
    _methods.put ("add", new java.lang.Integer (0));
    _methods.put ("query", new java.lang.Integer (1));
    _methods.put ("delete", new java.lang.Integer (2));
    _methods.put ("clear", new java.lang.Integer (3));
    _methods.put ("show", new java.lang.Integer (4));
    _methods.put ("save", new java.lang.Integer (5));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method,
                                org.omg.CORBA.portable.InputStream in,
                                org.omg.CORBA.portable.ResponseHandler $rh)
  {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ())
    {
       case 0:  // userModule/UserService/add
       {
         String startTime = in.read_string ();
         String endTime = in.read_string ();
         String item = in.read_string ();
         boolean $result = false;
         $result = this.add (startTime, endTime, item);
         out = $rh.createReply();
         out.write_boolean ($result);
         break;
       }

       case 1:  // userModule/UserService/query
       {
         String startTime = in.read_string ();
         String endTime = in.read_string ();
         String $result = null;
         $result = this.query (startTime, endTime);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 2:  // userModule/UserService/delete
       {
         String item = in.read_string ();
         boolean $result = false;
         $result = this.delete (item);
         out = $rh.createReply();
         out.write_boolean ($result);
         break;
       }

       case 3:  // userModule/UserService/clear
       {
         boolean $result = false;
         $result = this.clear ();
         out = $rh.createReply();
         out.write_boolean ($result);
         break;
       }

       case 4:  // userModule/UserService/show
       {
         String $result = null;
         $result = this.show ();
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 5:  // userModule/UserService/save
       {
         String name = in.read_string ();
         boolean $result = false;
         $result = this.save (name);
         out = $rh.createReply();
         out.write_boolean ($result);
         break;
       }

       default:
         throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  } // _invoke

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:userModule/UserService:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public UserService _this() 
  {
    return UserServiceHelper.narrow(
    super._this_object());
  }

  public UserService _this(org.omg.CORBA.ORB orb) 
  {
    return UserServiceHelper.narrow(
    super._this_object(orb));
  }


} // class UserServicePOA
