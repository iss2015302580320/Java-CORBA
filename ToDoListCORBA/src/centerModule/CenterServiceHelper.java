package centerModule;


/**
* centerModule/CenterServiceHelper.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从center.idl
* 2018年5月6日 星期日 下午10时36分06秒 CST
*/

abstract public class CenterServiceHelper
{
  private static String  _id = "IDL:centerModule/CenterService:1.0";

  public static void insert (org.omg.CORBA.Any a, centerModule.CenterService that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static centerModule.CenterService extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      __typeCode = org.omg.CORBA.ORB.init ().create_interface_tc (centerModule.CenterServiceHelper.id (), "CenterService");
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static centerModule.CenterService read (org.omg.CORBA.portable.InputStream istream)
  {
    return narrow (istream.read_Object (_CenterServiceStub.class));
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, centerModule.CenterService value)
  {
    ostream.write_Object ((org.omg.CORBA.Object) value);
  }

  public static centerModule.CenterService narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof centerModule.CenterService)
      return (centerModule.CenterService)obj;
    else if (!obj._is_a (id ()))
      throw new org.omg.CORBA.BAD_PARAM ();
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      centerModule._CenterServiceStub stub = new centerModule._CenterServiceStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

  public static centerModule.CenterService unchecked_narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof centerModule.CenterService)
      return (centerModule.CenterService)obj;
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      centerModule._CenterServiceStub stub = new centerModule._CenterServiceStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

}
