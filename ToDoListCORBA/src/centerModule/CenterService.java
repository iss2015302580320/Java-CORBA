package centerModule;


/**
* centerModule/CenterService.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从center.idl
* 2018年5月6日 星期日 下午10时36分06秒 CST
*/

public interface CenterService extends CenterServiceOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity 
{
} // interface CenterService
