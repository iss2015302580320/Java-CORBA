package centerModule;

/**
* centerModule/CenterServiceHolder.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从center.idl
* 2018年5月6日 星期日 下午10时36分06秒 CST
*/

public final class CenterServiceHolder implements org.omg.CORBA.portable.Streamable
{
  public centerModule.CenterService value = null;

  public CenterServiceHolder ()
  {
  }

  public CenterServiceHolder (centerModule.CenterService initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = centerModule.CenterServiceHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    centerModule.CenterServiceHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return centerModule.CenterServiceHelper.type ();
  }

}
