package centerModule;


/**
* centerModule/CenterServicePOA.java .
* 由IDL-to-Java 编译器 (可移植), 版本 "3.2"生成
* 从center.idl
* 2018年5月6日 星期日 下午10时36分06秒 CST
*/

public abstract class CenterServicePOA extends org.omg.PortableServer.Servant
 implements centerModule.CenterServiceOperations, org.omg.CORBA.portable.InvokeHandler
{

  // Constructors

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static
  {
    _methods.put ("login", new java.lang.Integer (0));
    _methods.put ("register", new java.lang.Integer (1));
    _methods.put ("save", new java.lang.Integer (2));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method,
                                org.omg.CORBA.portable.InputStream in,
                                org.omg.CORBA.portable.ResponseHandler $rh)
  {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ())
    {
       case 0:  // centerModule/CenterService/login
       {
         String name = in.read_string ();
         String password = in.read_string ();
         boolean $result = false;
         $result = this.login (name, password);
         out = $rh.createReply();
         out.write_boolean ($result);
         break;
       }

       case 1:  // centerModule/CenterService/register
       {
         String name = in.read_string ();
         String password = in.read_string ();
         boolean $result = false;
         $result = this.register (name, password);
         out = $rh.createReply();
         out.write_boolean ($result);
         break;
       }

       case 2:  // centerModule/CenterService/save
       {
         boolean $result = false;
         $result = this.save ();
         out = $rh.createReply();
         out.write_boolean ($result);
         break;
       }

       default:
         throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  } // _invoke

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:centerModule/CenterService:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public CenterService _this() 
  {
    return CenterServiceHelper.narrow(
    super._this_object());
  }

  public CenterService _this(org.omg.CORBA.ORB orb) 
  {
    return CenterServiceHelper.narrow(
    super._this_object(orb));
  }


} // class CenterServicePOA
