package impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.omg.CORBA.ORB;

import conf.Configuration;
import po.Item;
import po.User;
import userModule.UserServicePOA;

public class UserServiceImpl extends UserServicePOA {
	
	private ORB orb;
	private List<Item> userItemList ;

	public UserServiceImpl(String name) {
		init(name);
	}
	
	/**
	 * 想不明白，为什么这里的System.out永远都看不见？👀
	 * 为每一个user都保存一份userItemList到系统里，当用户登录要初始化的时候首先从序列化文件中
	 * @param userName
	 */
	public void init(String name) {
		FileInputStream fileInputStream=null;
		ObjectInputStream objectInpustStream=null;
		try {
			File dir = new File("data");
			if(!dir.exists()) {
				dir.mkdirs();
			}
			File file = new File("data"+File.separator + name);
			if(file.exists()) {
				 fileInputStream = new FileInputStream(file);
				 objectInpustStream = new ObjectInputStream(fileInputStream);
				 userItemList = (List<Item>) objectInpustStream.readObject();
				 return;
			}else {
				userItemList = new ArrayList<Item>();
			}
		}catch( IOException | ClassNotFoundException e) {
			e.printStackTrace();
			userItemList = new ArrayList<Item>();
		}finally {
			try {
				if(objectInpustStream!=null)
					objectInpustStream.close();
				if(fileInputStream!=null)
					fileInputStream.close();
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("init "+name+" items"+" failed");
	}
	
	@Override
	public boolean save(String name) {
		FileOutputStream fileOutputStream = null;
		ObjectOutputStream objectOutputStream = null;
		try {
			File dir = new File("data");
			if(!dir.exists()) {
				dir.mkdirs();
			}
			File file = new File("data"+File.separatorChar+name);
			fileOutputStream = new FileOutputStream(file);
			 objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(this.userItemList);
			objectOutputStream.flush();
			fileOutputStream.flush();
			return true;
		}catch(IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if(objectOutputStream!=null) {
					objectOutputStream.close();
				}
				if(fileOutputStream!=null) {
					fileOutputStream.close();
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			
		}
		return false;
	}
	
	
	public void setORB(ORB orb_val) {
	 orb = orb_val; 
	}
	@Override
	public boolean add(String startTime, String endTime, String value) {
		Item item = new Item(startTime,endTime,value);
		this.userItemList.add(item);
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String query(String startTime, String endTime) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("startTime")
		.append("\t")
		.append("endTime")
		.append("\t")
		.append("item")
		.append("\n");
		for(Item item:this.userItemList) {
			if(item.getStartTime().compareTo(startTime) >= 0  && item.getEndTime().compareTo(endTime)<=0) {
				sb.append(item.toString())
				.append("\n");
				
			}
		}
		return sb.toString();
	}

	@Override
	public boolean delete(String item) {
		// TODO Auto-generated method stub
		for(int i=0;i<this.userItemList.size();) {
			if(userItemList.get(i).getValue().equals(item)) {
				userItemList.remove(i);
				return true;
			}else {
				i++;
			}
		}
		return false;
	}

	@Override
	public boolean clear() {
		this.userItemList.clear();
		return true;
	}

	@Override
	public String show() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("startTime")
		.append("\t")
		.append("endTime")
		.append("\t")
		.append("item")
		.append("\n");
		for(Item item : this.userItemList) {
			sb.append(item.toString())
			.append("\n");
		}
		return sb.toString();
	}

}
