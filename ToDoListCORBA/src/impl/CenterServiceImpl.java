package impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.omg.CORBA.ORB;

import centerModule.CenterServicePOA;
import conf.Configuration;
import po.User;
import server.ToDoListServer;

public class CenterServiceImpl extends CenterServicePOA {

	/**
	 * 管理所有的用户
	 */
	private Map<String,User> userMap ;

	private ToDoListServer server;
	
	public void  setServer(ToDoListServer server) {
		this.server = server;
	}
	

	public CenterServiceImpl() {
		init();
	}
	
	/**
	 * 初始化userMap
	 */
	public void init() {
		FileInputStream fileInputStream=null;
		ObjectInputStream objectInpustStream=null;
		try {
			File file = new File("data"+File.separator+Configuration.getConfig("service.center.name"));
			if(file.exists()) {
				 fileInputStream = new FileInputStream(file);
				 objectInpustStream = new ObjectInputStream(fileInputStream);
				userMap = (Map<String,User>) objectInpustStream.readObject();
				 System.out.println("init centerService userMap"+" successfully");
			}else {
				userMap = new HashMap<>();
				System.out.println("init centerService userMap"+" failed");
			}
		}catch( IOException | ClassNotFoundException e) {
			e.printStackTrace();
			userMap = new HashMap<String,User>();
		}finally {
			try {
				if(objectInpustStream!=null)
					objectInpustStream.close();
				if(fileInputStream!=null)
					fileInputStream.close();
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private ORB orb;

	public void setORB(ORB orb_val) {
	 orb = orb_val; 
	}
	
	
	@Override
	public boolean login(String name, String password) {
		// TODO Auto-generated method stub
		User user  = userMap.get(name);
		if(user==null) {
			return false;
		}else if(Objects.equals(user.getPassword(), password)) {
			UserServiceImpl userService = new UserServiceImpl(name);
			userService.save(name);
			this.save();
			this.server.registerService(name);
			return true;
		}else  return false;
	}

	@Override
	public boolean register(String name, String password) {
		// TODO Auto-generated method stub
		if(userMap.containsKey(name)) {
			return false;
		}else {
			userMap.put(name, new User(name,password));
			UserServiceImpl userService = new UserServiceImpl(name);
			userService.save(name);
			this.save();
			this.server.registerService(name);
			return true;
		}
	}
	
	/**
	 * 存储userMap到硬盘
	 */
	@Override
	public boolean save() {
		FileOutputStream fileOutputStream = null;
		ObjectOutputStream objectOutputStream = null;
		try {
			File dir = new File("data");
			if(!dir.exists()) {
				dir.mkdirs();
			}
			File file = new File("data"+File.separatorChar+Configuration.getConfig("service.center.name"));
			fileOutputStream = new FileOutputStream(file);
			 objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(userMap);
			objectOutputStream.flush();
			fileOutputStream.flush();
			return true;
		}catch(IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if(objectOutputStream!=null) {
					objectOutputStream.close();
				}
				if(fileOutputStream!=null) {
					fileOutputStream.close();
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			
		}
		return false;
	}
	
}
